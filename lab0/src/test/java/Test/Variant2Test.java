package Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import lab0.Variant2;



import static org.testng.Assert.*;
public class Variant2Test {
    @Test(dataProvider = "providerBeginTask")
    public void testBeginTask(double a, double result) {
        assertEquals(new Variant2().beginTask(a), result);
    }

    @DataProvider
    public Object[][] providerBeginTask() {
        return new Object[][]{{5, 25}, {1.5, 2.25}};
    }

    //--------------------------------------------------------------------------------------------
    @Test(expectedExceptions = AssertionError.class, dataProvider = "providerErrorBeginTask")
    public void testErrorBeginTask(double a) {
        new Variant2().beginTask(a);
    }

    @DataProvider
    public Object[][] providerErrorBeginTask() {
        return new Object[][]{{-3}, {-2}};
    }


    //------------------------------- Test IntegerTask -------------------------------
    @Test(dataProvider = "providerIntegerTask")
    public void testIntegerTask(double ton, double result) {
        assertEquals(new Variant2().integerTask(ton), result);
    }

    @DataProvider
    public Object[][] providerIntegerTask() {
        return new Object[][]{{5000, 5}, {1500, 1}};
    }

    //--------------------------------------------------------------------------------------------------
    @Test(expectedExceptions = AssertionError.class, dataProvider = "providerErrorIntegerTask")
    public void testErrorIntegerTask(double ton) {
        new Variant2().beginTask(ton);
    }

    @DataProvider
    public Object[][] providerErrorIntegerTask() {
        return new Object[][]{{-3}};
    }

    //  ------------------------------- Test BooleanTask -------------------------------
    @Test(dataProvider = "booleanProvider")
    public void booleanTest(int num, boolean rez) {
        assertEquals(new Variant2().booleanTask(num), rez);
    }

    @DataProvider
    public Object[][] booleanProvider() {
        return new Object[][]{{2, false}, {0, false}, {5, true}};
    }

    //  ------------------------------- Test IfTask -------------------------------
    @Test(dataProvider = "ifProvider")
    public void ifTest(int numb, int rez) {
        assertEquals(new Variant2().ifTask(numb), rez);
    }

    @DataProvider
    public Object[][] ifProvider() {
        return new Object[][]{{0, -2}, {4, 5}, {-1, -3}};
    }

    //  ------------------------------- Test CaseTask -------------------------------
    @Test(dataProvider = "caseProvider")
    public void caseTest(int index, String mark) {
        assertEquals(new Variant2().caseTask(index), mark);
    }

    @DataProvider
    public Object[][] caseProvider() {
        return new Object[][]{{1, "bad"}, {2, "poor"}};
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "providerErrorCaseTask")
    public void testErrorCaseTask(int index) {
        new Variant2().caseTask(index);
    }

    @DataProvider
    public Object[][] providerErrorCaseTask() {
        return new Object[][]{{7}, {-2}};
    }

    //  ------------------------------- Test ForTask -------------------------------
    @Test(dataProvider = "forProvider")
    public void forTest(double X, int N, double result) {
        assertEquals(new Variant2().forTask(X, N), result);
    }

    @DataProvider
    public Object[][] forProvider() {
        return new Object[][]{{1, 1, 1}};
    }

    //--------------------------------------------------------------------------------------------
    @Test(expectedExceptions = AssertionError.class, dataProvider = "providerErrorForTask")
    public void testErrorForTask(double X, int N) {
        new Variant2().forTask(X, N);
    }

    @DataProvider
    public Object[][] providerErrorForTask() {
        return new Object[][]{{-3, -1}, {1, -2}};
    }

    //--------------------------------- Test WhileTask -----------------------------------------------
    @Test(dataProvider = "whileProvider")
    public void whileTest(int A, int B, int res) {
        assertEquals(new Variant2().whileTask(A, B), res);
    }

    @DataProvider
    public Object[][] whileProvider() {
        return new Object[][]{{11, 6, 1}, {9, 5, 1}};
    }

    //-------------------------------------------------------------------------------------------------
    @Test(expectedExceptions = AssertionError.class, dataProvider = "providerErrorForTask")
    public void testErrorWhileTask(int A, int B) {
        new Variant2().forTask(A, B);
    }

    @DataProvider
    public Object[][] providerErrorWhileTask() {
        return new Object[][]{{2, 4}, {0, -2}};
    }

    //------------------------------ Test ArrayTask -----------------------------------------
    @Test(dataProvider = "arrayProvider")
    public void arrayTest(int[] array, int res) {
        assertEquals(new Variant2().arrayTask(array), res);
    }

    @DataProvider
    public Object[][] arrayProvider() {
        return new Object[][]{{new int[]{1, 4, 2, 1, 8, 5, -7, -10, -8, 12}, 5}};
    }

    //------------------------ Test MatrixTask ----------------------------
    @Test(dataProvider = "matrixProvider")

    public void MatrixTest(int K1, int K2, int[][] input, int[][] output) {
        assertTrue(equals(new Variant2().MatrixTask(K1, K2, input), output));

    }
    private boolean equals(int[][] matrix, int[][] result) {
        if(matrix.length!=result.length)return false;
        else
        {

            int N = matrix.length;
            int M = matrix[0].length;
            boolean res;
            for( int i=0; i < N;i++)
            {

                for( int j=0; j < M ; j++)
                {
                    if(matrix[i][j]!= result[i][j])
                    {
                        return false;


                    }
                }

            }
        }
        return true;
    }

    @DataProvider
    public Object[][] matrixProvider() {
        int K1=1;
        int K2=3;
        int[][] input = {{1, 2, 3, 4, 5},
                {6, 7, 8, 9, 10},
                {11, 12, 13, 14, 15},
                {16, 17, 18, 19, 10}};

        int[][] output = {{1, 4, 3, 2, 5},
                {6, 9, 8, 7, 10},
                {11, 14, 13, 12, 15},
                {16, 19, 18, 17, 10}};
        return new Object[][]{{K1,K2, input, output}};
    }
}
