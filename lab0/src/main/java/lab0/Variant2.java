package lab0;


public class Variant2
 {
     /**
      * @param a is a number
      * @return the square of number A
      */
     public double beginTask(double a)
    {
        assert(a > 0): "a should be greater than 0";
        return a*a;
    }

//  ------------------------------- IntegerTask -------------------------------

     /**
      *
      * @param ton is a number
      * @return the fraction of ton and 1000
      */
     public double integerTask(double ton)
     {
         assert(ton > 0):"tons can't have a negative value";
         return (int)ton/1000;
     }
    //  ------------------------------- BooleanTask -------------------------------
    public boolean booleanTask(int num)
    {
        return (num%2!=0);
    }
     //  ------------------------------- IfTask -------------------------------
     public int ifTask(int numb){
         if (numb>0)
             return numb+1;
         else
         return numb-2;
     }
     //  ------------------------------- CaseTask -------------------------------

     /**
      *
      * @param index is a number
      * @return the mark
      */
     public String caseTask(int index) {
         assert (index > 0 && index < 6) : "Error";

         String mark = " ";
         switch (index) {
             case 1:
                  mark = "bad";
                 break;
             case 2:
                 mark = "poor";
                 break;
             case 3:
                 mark = "meduim";
                 break;
             case 4:
                 mark = "good";
                 break;
             case 5:
                 mark = "excellent";
                 break;
         }
         return mark;
     }
     //  ------------------------------- ForTask -------------------------------
     public static int calculateFactorial(int N){
         int result = 1;
         for (int i = 1; i <=N; i ++){
             result = result*i;
         }
         return result;
     }

     /**
      *
      * @param X is a number of a row
      * @param N is a a power of number
      * @return the sum of a row
      */
     public static double forTask(double X,int N)
     {
         assert (N>0);
         double totalValue = 0;
         double numerator = 1;
         int sign = 1;
         int multiplier = 0;
          for(int i = 0;i < N;++i)
          {
            totalValue += sign*(numerator/calculateFactorial(multiplier));
            numerator *= X;
            sign *= 1;
            multiplier +=1;
          }
          return totalValue;
     }
     //  ------------------------------- WhileTask -------------------------------

     /**
      *
      * @param A is a length of the longest line
      * @param B is a length of shorter segment
      * @return the number of segments B on A
      */
      public int whileTask(int A,int B)
     {
         int k = 0;
         assert ((A >= 0)&&(B >= 0)) : "Argument should be more than or =  zero";
         assert A >= B : "Argument A should be more than argument B";
         while((A>=0)&&(B>=0)&&(A>B))
          {
              A=A-B;
              k++;
          }
         return k;
     }
     //  ------------------------------- ArrayTask -------------------------------

     /**
      *
      * @param array is an array of integer
      * @return the position of the last number that satisfies the conditions A[0]<A[i]<A[10]
      */
     public int arrayTask(int[] array)
     {

         int position = 0;

         for(int i = 8; i > 0; i--)
         {
           if((array[i] > array[0]) && (array[i] < array[9])) {
               position = i;
               return position;
           }
         }

         return 0;

     }
     //-------------------------- MatrixTask ------------

     /**
      *
      * @param K1 is a number of integer
      * @param K2 is a number of integer
      * @param mat is a matrix
      * @return changes the places of K1 and K2 columns
      */
     public int[][] MatrixTask(int K1,int K2,int[][] mat) {
         assert K1 < K2:"K1 should be lesser than K2";
         int temp;
         for (int i = 0; i < mat.length; i++)
         {
             temp = mat[i][K1];
             mat[i][K1] = mat[i][K2];
             mat[i][K2] = temp;
         }
         return mat;
     }
 }
